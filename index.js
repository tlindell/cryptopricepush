var ipaddress = process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1";
var port      = process.env.OPENSHIFT_NODEJS_PORT || 8080;
var dbname    = "nodejs";

var WebSocketServer = require('ws').Server
var http = require('http');
var jquery = require('jquery');

var MongoClient = require('mongodb').MongoClient;
// Connect to the db
MongoClient.connect("mongodb://"+ipaddress+":"+port+"/"+dbname, function(err, db) {
  if(!err) {
    console.log("We are connected");
  }
  else{
    console.log("Err: "+err);
  }
});
// var mongoose = require('mongoose');

var lastPrice = [];



/*=====================================
=            set up server            =
=====================================*/

var server = http.createServer(function(request, response) {
    console.log((new Date()) + ' Received request for ' + request.url);
    
    response.writeHead(200, { 'Content-Type': 'text/html' });
    response.write('<!DOCTYPE html><html ><head>');
    response.write('<meta charset="utf-8">');
    response.write('<title>' + 'Yay Node!' + '</title>');
    response.write('<link rel=stylesheet href=../styles/styles.css rel=stylesheet />');
    response.write('<script src=script.js type=text/javascript></script>');
    response.write('</head><body>');

    response.write('<h1><tt>'+lastPrice[4]+'</tt></h1>');
    response.write('<script type="text/javascript">test()</script>')
    //response.write('<script type="text/javascript">script.onload = function () {    alert("from html Node!")}; </script>')
    response.write('<input id="refresh" type="button" value="refresh" onclick="location.reload()" />')
    response.write("<ul class='lastPrice'><li>"+lastPrice[0]+"</li><li>"+lastPrice[1]+"</li><li>"+lastPrice[2]+"</li><li>"+lastPrice[3]+"</li><ul>");
    response.write('</body></html>');
    response.end();
});

// mongoose.connect('mongodb://'+ipaddress+':'+port+'/'+dbname);

server.listen( port, ipaddress, function() {
    console.log((new Date()) + ' Server is listening on port' + port);
});

wss = new WebSocketServer({
    server: server,
    autoAcceptConnections: false
});
wss.on('connection', function(ws) {
  console.log("New connection");
  ws.on('message', function(message) {
    ws.send("Received: " + message);
  });
  ws.send('Welcome!');
});

console.log("Listening to " + ipaddress + ":" + port + "...");


/*-----  End of set up server  ------*/









var options = {
  host: 'pubapi1.cryptsy.com',
  path: '/api.php?method=singlemarketdata&marketid=182'
};



callback = function(response) {
  var str = '';

  //another chunk of data has been recieved, so append it to `str`
  response.on('data', function (chunk) {
    if(chunk.length > 10){
      str += chunk;
    }
  });

  //the whole response has been recieved, so we just print it out here
  response.on('end', function () {
    var dogePrice = JSON.parse(str).return.markets.DOGE.lasttradeprice, 
        dogeBal = 218580.52211297, 
        cashBal = 91.42675889, 
        totalBal = (dogePrice*dogeBal)+cashBal,
        tradeTime = JSON.parse(str).return.markets.DOGE.lasttradetime;

    if(str.length > 10000){
    	if((lastPrice[0] != JSON.parse(str).return.markets.DOGE.lasttradeprice)){
  	    	lastPrice.length = 0;
          if(JSON.parse(str).return.markets.DOGE.lasttradeprice.indexOf('}') == -1){
    		  	lastPrice.push(JSON.parse(str).return.markets.DOGE.lasttradeprice);
    		  	lastPrice.push(dogeBal);
    		  	lastPrice.push(cashBal);
            lastPrice.push(totalBal);
    		  	lastPrice.push(tradeTime);
    		    console.log(lastPrice);
            // response.write("<script>document.getElementById('lastPrice').appendChild(lastPrice);</script>")
           
            // response.write(lastPrice+"\n\n");
          }
    	}
    }

  });
}

setInterval(function(){

  var request = http.request(options, callback);

  request.on('error', function(err) {
    var msg = "There was an error "+err;
    if((lastPrice[0] != msg)){
      lastPrice.length = 0;
      lastPrice.push(msg);
      console.log(lastPrice[0]);
    }
  });

  request.end();

},1000);

